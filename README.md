#WAVE2MP3

A simple program to convert wave files to mp3 files.
  - Wave files should be contained in a directory which can be passed as in input argument to the program

# Steps for Compilation (Linux)

```sh
$ sudo apt-get update
$ sudo apt-get install build-essential -y
$ g++ -v
...
gcc version 7.3.0
$ make
```

# Steps for Compilation (Windows)

  - Download codeblocks-17.12mingw-setup.exe for Windows from http://www.codeblocks.org/downloads/binaries
  - This file includes GCC/G++ compiler which will be installed in "C:\Program Files (x86)\CodeBlocks\MinGW"
  - Now open command prompt and execute "C:\Program Files (x86)\CodeBlocks\MinGW\mingwvars.bat"
  
```sh
$ C:\Program Files (x86)\CodeBlocks\MinGW\mingwvars.bat
$ mingw32-make clean
$ mingw32-make
```

# Execution

```
$ wave2mp3 /some/path/to/wave/files
```
