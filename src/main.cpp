#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include "lame.h"
#include <string>
#include <thread>
#include <pthread.h>
#include <list>
using namespace std;

static pthread_mutex_t local_mutex = PTHREAD_MUTEX_INITIALIZER;
std::list<std::string> waveFilesList;

const char kPathSeparator =
#if defined _WIN32
    '\\';
#else
    '/';
#endif

struct ThreadArgs
{
    int id;
    string path;
};

int waveToMp3(string src, string target)
{
    lame_global_flags *gfp;
    gfp = lame_init();

    lame_set_num_channels(gfp, 2);
    lame_set_in_samplerate(gfp, 44100);
    lame_set_brate(gfp, 128);
    lame_set_quality(gfp, 3);

    const int PCM_SIZE = 1024 * 1024;
    const int MP3_SIZE = 1024 * 1024;
    short int pcm_buffer[PCM_SIZE * 2];
    unsigned char mp3_buffer[MP3_SIZE];
    FILE *fp, *mp3;
    int read, write;

    if ((fp = fopen(src.c_str(), "rb")) == NULL)
    {
        perror("Error opening data file");
        return -1;
    }

    if ((mp3 = fopen(target.c_str(), "wb")) == NULL)
    {
        perror("Opening output file");
        return -1;
    }

    if (lame_init_params(gfp) == -1)
    {
        printf("Something failed in setting internal parameters.");
    }

    do
    {
        read = fread(pcm_buffer, 2 * sizeof(short int), PCM_SIZE, fp);
        if (read == 0)
        {
            write = lame_encode_flush(gfp, mp3_buffer, MP3_SIZE);
        }
        else
        {
            write = lame_encode_buffer_interleaved(gfp, pcm_buffer, read, mp3_buffer, MP3_SIZE);
        }
        fwrite(mp3_buffer, 1, write, mp3);

    } while (read != 0);

    fclose(mp3);
    fclose(fp);
    lame_close(gfp);

    return 0;
}

void *worker(void *arg)
{
    ThreadArgs ta = *((ThreadArgs *)arg);
    while (!waveFilesList.empty())
    {
        string wavefilename, mp3filename;
        pthread_mutex_lock(&local_mutex);
        if (!waveFilesList.empty())
        {
            wavefilename = waveFilesList.front();
            waveFilesList.pop_front();
        }
        pthread_mutex_unlock(&local_mutex);
        if (wavefilename.length() > 0)
        {
            mp3filename = wavefilename.substr(0, wavefilename.length() - 4) + ".mp3";
            waveToMp3(ta.path + kPathSeparator + wavefilename, ta.path + kPathSeparator + mp3filename);
            printf("%d converted %s\n", ta.id, wavefilename.c_str());
        }
    }
    return nullptr;
}

int main(int argc, char *argv[])
{
    unsigned int nthreads = std::thread::hardware_concurrency();
    ThreadArgs thread_args[nthreads];
    pthread_t threads[nthreads];

    if (argc != 2)
    {
        perror("Usage: wave2mp3 <directory containing wave files>");
        return -1;
    }

    DIR *dir;
    string path = string(argv[1]);
    dir = opendir(argv[1]);

    if (dir == NULL)
    {
        perror("failed to open dir");
        return -1;
    }

    struct dirent *entry;
    struct stat fileStat;

    while ((entry = readdir(dir)))
    {
        std::string fileName = entry->d_name;
        std::string filePath = path + kPathSeparator + fileName;
        stat(filePath.c_str(), &fileStat);
        if (fileStat.st_mode & S_IFREG)
        {
            waveFilesList.push_back(fileName);
        }
    }

    closedir(dir);

    for (unsigned int i = 0; i < nthreads; ++i)
    {
        ThreadArgs ta;
        ta.id = i;
        ta.path = string(path);
        thread_args[i] = ta;
        int result_code = pthread_create(&threads[i], 0, worker, (void *)&thread_args[i]);
        if (result_code != 0)
        {
            perror("Failed to create thread!");
            return -1;
        }
    }

    for (unsigned int i = 0; i < nthreads; ++i)
    {
        pthread_join(threads[i], 0);
    }
}
