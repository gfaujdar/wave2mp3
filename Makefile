GCC := g++
CFLAGS = -I include
BIN_NAME = wave2mp3

BUILD_DIR = ./build
SRCS = src/main.cpp

ifeq ($(OS),Windows_NT)
	LFLAGS = -L libs/win
else
	LFLAGS = -L libs/unix
endif

LIBS = -lmp3lame -lpthread

wave2mp3: $(SRCS)
	$(GCC) -std=c++14 $(CFLAGS) -o wave2mp3 $(SRCS) $(LFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f $(BUILD_DIR)/*.* wave2mp3 wave2mp3.exe
